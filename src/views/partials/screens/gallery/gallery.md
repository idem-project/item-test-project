![gallery](./gallery.png "скриншот скрина gallery")

// *gallery*
# блок галерея картинок

пропсы:
- gallery: object, хранит информацию скрина gallery
  - title: string, загаловок скрина gallery
  - slider: array, хранит информацию слайдов для слайдера
    - src: string, путь до картинки
    - alt: string, альтернативное название картинки
    - description: описание слайда gallery
    

пример данных:
```
"gallery": {
  "title": "жизнь в жилом микрорайоне Одинбург",
  "slider": [
    {
      "src": "/assets/img/gallery_dvor-rellax.jpg",
      "alt": "cool-room",
      "title": "двор и места отдыха",
      "source": [
        {
          "srcset": "/assets/img/gallery_dvor-rallax_375.jpg",
          "media": "max-width: 768px"
        }
      ]
    },
  ]
}
```
модификаторы:
