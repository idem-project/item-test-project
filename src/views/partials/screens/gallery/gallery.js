import Swiper, {Navigation, Pagination} from "swiper";

const sliderGalleryInit = (parent) => {

  if (!parent) return

  new Swiper(parent, {
    modules: [Navigation, Pagination],
    speed: 400,
    navigation: {
      nextEl: ".gallery__slider-btn-next",
      prevEl: ".gallery__slider-btn-prev",
    },
    centeredSlides: true,
    slidesPerView: 'auto',
    pagination: {
      el: ".gallery__slider-pagination",
      type: "fraction",
    },
  });
}

export default sliderGalleryInit;
