# card-chars
![card](./card.png 'скриншот слайда')

пропсы:
- heading: object, хранит данные для загаловка карточки
- img: object, данные о картинке на карточке
  - src: string, путь к картинке
  - alt: string, альтернативный текст картинки
- items: array, массив характеристик для листа карточки
  - title: string, текст характеристики - большой
  - value: string, текст характеристики поменьше
- class: string, дополнительный кдасс для отступов и позиционирования

пример данных:
```
"heading": {
  "title": "функциональные планировки",
  "labels": [
    {
      "tag": "квартиры в одинбурге"
    }
  ]
},
"img": {
  "src": "/assets/img/slider-image_1.jpg",
  "alt": "cool-room"
},
"items": [
  {
    "value": "площади квартир",
    "title": "24-83 м²"
  }
],
class: 'card-chars__hidding'
```
модификаторы:
